FROM openjdk:11

COPY ./ ./

EXPOSE 8080

ENV CLEARDB_DATABASE_URL mysql://b0e18327faa319:ed186abc@eu-cdbr-west-03.cleardb.net/heroku_237ca23bce2da72?useUnicode=true&characterEncoding=utf-8&reconnect=true

CMD ["java", "-jar", "target/meshkovo-new-1.0.jar"]
